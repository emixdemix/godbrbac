package constants

import (
	"errors"
)

const (
	ERR_USER_EXISTS = iota
	ERR_USER_NOT_FOUND
	ERR_PASSWORD_ERROR
	ERR_ROLE_EXISTS
	ERR_PERMISSION_EXISTS
	ERR_ROLE_DOES_NOT_EXIST
	ERR_PERMISSION_DOES_NOT_EXIST
	ERR_PERMISSION_HAS_ROLE
	WRONG_PASSWORD
)

type ErrorMessages struct {
	Id      int
	Message string
}

var ErrorList []ErrorMessages = make([]ErrorMessages, 0)

func AddError(id int, message string) {
	var errorMessage = ErrorMessages{}
	errorMessage.Id = id
	errorMessage.Message = message
	ErrorList = append(ErrorList, errorMessage)
}

func New(errorId int) error {
	return errors.New(ErrorList[errorId].Message)
}

func init() {
	AddError(ERR_USER_EXISTS, "User already exist")
	AddError(ERR_USER_NOT_FOUND, "User not found")
	AddError(ERR_PASSWORD_ERROR, "Hashing password error")
	AddError(ERR_ROLE_EXISTS, "Role exists")
	AddError(ERR_PERMISSION_EXISTS, "Permission exists")
	AddError(ERR_ROLE_DOES_NOT_EXIST, "Role does not exist")
	AddError(ERR_PERMISSION_DOES_NOT_EXIST, "Permission does not exist")
	AddError(ERR_PERMISSION_HAS_ROLE, "Permission belongs to a Role")
	AddError(WRONG_PASSWORD, "Wrong username or password")

}
