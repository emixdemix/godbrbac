/*
Package GODBRBAC provides a role-based framework with plug in engines
for different storage systems. Out of the box provides MySql databse engine
using GORM.

The idea is to simplify the managing of role based access and storage:
	* an identity has one or more roles.
	* a role has one or more permission.

*/
package godbrbac

import (
	"bitbucket.org/emixdemix/godbrbac/constants"
	"golang.org/x/crypto/bcrypt"
)

type Permissions struct {
	PermissionId uint `gorm:"primary_key"`
	Name         string
}

type Roles struct {
	RoleId     uint `gorm:"primary_key"`
	Name       string
	Permission []Permissions `gorm:"many2many:role_permissions;"`
}

type Identities struct {
	IdentityId uint `gorm:"primary_key"`
	Username   string
	Password   string
	Name       string
	Role       []Roles `gorm:"many2many:identitie_roles;"`
}

// This is the Engine interface that needs to be satisfied by your engine.
type Engine interface {
	Init(connectionString string)

	CreatePermission(permission *Permissions) error
	FindPermissionByName(permissionName string, perm *Permissions)
	CheckRolesWithPermission(permission *Permissions) int
	DeletePermission(permission *Permissions) error

	CreateRole(role *Roles) error
	UpdateRole(role *Roles) error
	FindRoleByName(roleName string, role *Roles)
	DeleteRole(role *Roles) error

	CreateIdentity(id *Identities) error
	UpdateIdentity(id *Identities) error
	DeleteIdentity(id *Identities) error
	FindIdentityByUsername(username string, id *Identities) error

	CreateTables()
}

// Global variable to store the engine
var factoryDb Engine

/* *********************************** BEGIN ***********************/

//Init accept an interface of type Engine, and a coonection string which may be useful
//for database storage engines
func Init(connectionString string, engine Engine) {
	factoryDb = engine
	factoryDb.Init(connectionString)
}

// Adds a Permission to the Permissions storage area. Checks only if NAME already exists.
// Two permissions with same name cannot cohexist
func AddRbacPermission(permissionName string) (perms Permissions, err error) {
	var permission = Permissions{}
	factoryDb.FindPermissionByName(permissionName, &permission)

	if permission.Name == permissionName {
		return permission, constants.New(constants.ERR_PERMISSION_EXISTS)
	}

	permission.Name = permissionName
	err = factoryDb.CreatePermission(&permission)

	return permission, err
}

// Deletes a Permission from the Permissions storage area.
// Before deleting the perission, it checks whether it has a role
// which owns it.
func DeleteRbacPermission(permissionName string) (err error) {
	var permission = Permissions{}

	factoryDb.FindPermissionByName(permissionName, &permission)

	if permission.Name != permissionName {
		return constants.New(constants.ERR_PERMISSION_DOES_NOT_EXIST)
	}

	count := factoryDb.CheckRolesWithPermission(&permission)

	if count <= 0 {
		err = factoryDb.DeletePermission(&permission)
	} else {
		return constants.New(constants.ERR_PERMISSION_HAS_ROLE)
	}

	return err
}

// Adds a Role to the Roles storage. Checks only if Name already exists.
// The Role is created with an empty set of Permissions.
func AddRbacRole(roleName string) (roles Roles, err error) {
	var role = Roles{}

	factoryDb.FindRoleByName(roleName, &role)

	if role.Name == roleName {
		return role, constants.New(constants.ERR_ROLE_EXISTS)
	}

	role.Name = roleName

	err = factoryDb.CreateRole(&role)

	return role, err
}

// Deletes a Role from the Roles storage. Checks only if Name exists.
func DeleteRbacRole(roleName string) (err error) {
	var role = Roles{}

	factoryDb.FindRoleByName(roleName, &role)

	if role.Name != roleName {
		return constants.New(constants.ERR_ROLE_DOES_NOT_EXIST)
	}

	err = factoryDb.DeleteRole(&role)

	return err
}

// Given a Name, it returns a Role object.
func GetRbacRole(roleName string) (role Roles, err error) {
	factoryDb.FindRoleByName(roleName, &role)
	if role.Name == roleName {
		return role, nil
	}
	return role, constants.New(constants.ERR_ROLE_DOES_NOT_EXIST)
}

// Given a Name, it returns a Permissions object.
func GetRbacPermission(permissionName string) (perms Permissions, err error) {
	factoryDb.FindPermissionByName(permissionName, &perms)
	if perms.Name == permissionName {
		return perms, nil
	}
	return perms, constants.New(constants.ERR_PERMISSION_DOES_NOT_EXIST)
}

// Adds a set of permissions to a Role. The set contains an array of
// Names, and if the permission with that name exists, it will be addes
// to the Role. Otherwise it will be skipped.
func AddPermissions(role *Roles, perms []string) (err error) {
	var permissionList = make([]Permissions, 0)
	for _, item := range perms {
		var perm = Permissions{}
		factoryDb.FindPermissionByName(item, &perm)
		if perm.Name == item {
			permissionList = append(permissionList, perm)
		}
	}

	role.Permission = permissionList
	ret := factoryDb.UpdateRole(role)

	return ret
}

// Adds an Identity to the Identity storage area. Checks only if username is
// used already. It returns an Identity object and an error if any.
func AddIdentity(id *Identities) (identity Identities, err error) {
	var temp = Identities{}
	factoryDb.FindIdentityByUsername(id.Username, &temp)
	if temp.Username == id.Username {
		return temp, constants.New(constants.ERR_USER_EXISTS)
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(id.Password), bcrypt.DefaultCost)
	if err != nil {
		return *id, constants.New(constants.ERR_PASSWORD_ERROR)
	}
	id.Password = string(hashedPassword)

	ret := factoryDb.CreateIdentity(id)
	//	ret := db.Db.Save(id)

	return *id, ret
}

// Deletes an Identity from the Identity storage area. It returns an error if any.
func DeleteIdentity(id *Identities) error {
	var temp = Identities{}
	factoryDb.FindIdentityByUsername(id.Username, &temp)
	if temp.Username == id.Username {
		ret := factoryDb.DeleteIdentity(&temp)
		return ret
	}

	return constants.New(constants.ERR_USER_NOT_FOUND)
}

// Adds roles to an Identity. If the role does not exist it will be skipped.
func AddRoles(id *Identities, roles []string) (err error) {
	var temp = Identities{}
	var roleList = make([]Roles, 0)
	ret := factoryDb.FindIdentityByUsername(id.Username, &temp)
	if ret != nil {
		return ret
	}
	if temp.Username == id.Username {
		for _, item := range roles {
			r, err := GetRbacRole(item)
			if err == nil {
				roleList = append(roleList, r)
			}
		}

		temp.Role = roleList

		ret := factoryDb.UpdateIdentity(&temp)
		return ret
	}

	return constants.New(constants.ERR_USER_NOT_FOUND)
}

// Returns a set of permission and true if the Identoty has that role.
// Nil and false otherwise
func HasRole(id *Identities, role string) (perms []Permissions, has bool) {
	for _, item := range id.Role {
		if item.Name == role {
			return item.Permission, true
		}
	}

	return nil, false
}

// Returns true if an Identity has a role and a permission. False otherwise.
func HasRolePermission(id *Identities, role string, permission string) (has bool) {
	perms, has := HasRole(id, role)
	if has {
		for _, item := range perms {
			if item.Name == permission {
				return true
			}
		}
	}
	return false
}

// Returns true if an Identity has a permission. False otherwise.
func HasPermission(id *Identities, permission string) (has bool) {
	for _, item := range id.Role {
		for _, perm := range item.Permission {
			if perm.Name == permission {
				return true
			}
		}
	}
	return false
}

// Given a username and a password, returns an Identity object if a match is found.
// Error message if either one does not match.
func Login(username, password string) (id Identities, err error) {
	var temp = Identities{}

	ret := factoryDb.FindIdentityByUsername(username, &temp)
	if ret != nil {
		return temp, constants.New(constants.ERR_USER_NOT_FOUND)
	}

	match := bcrypt.CompareHashAndPassword([]byte(temp.Password), []byte(password))

	if temp.Username == username && match == nil {
		return temp, nil
	}

	return temp, constants.New(constants.WRONG_PASSWORD)
}

// Given a username returns an Identity object if a match is found.
// Error message if one does not match.
func LoginExternal(username string) (id Identities, err error) {
	var temp = Identities{}

	ret := factoryDb.FindIdentityByUsername(username, &temp)
	if ret != nil {
		return temp, constants.New(constants.ERR_USER_NOT_FOUND)
	}

	return temp, nil
}

// If the storage area needs to be initialized, this is the place to do that.
func CreateTables() {
	factoryDb.CreateTables()
}
