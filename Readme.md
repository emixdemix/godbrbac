goDBRBAC 
======

__NOTE:__

0. Work still in progress however functional.

1. Use at your own __RISK__.

[![GoDoc](https://godoc.org/github.com/mikespook/gorbac?status.png)](https://godoc.org/github.com/mikespook/gorbac)
[![Coverage Status](https://coveralls.io/repos/github/mikespook/gorbac/badge.svg?branch=master)](https://coveralls.io/github/mikespook/gorbac?branch=master)

goDBRBAC provides a lightweight role-based access control implementation
in Golang with associated persistent gorm framework.

For the purposes of this package:

	* an identity has one or more roles.
	* a role has one or more permissions.

goDBRBAC has the following model:

	* many to many relationship between identities and roles.
	* many to many relationship between roles and permissions.

Version
=======

Currently, goDBRBAC has one version:

[Version 1](https://bitbucket.com/emixdemix/godbrbac/) 


Install
=======

Install the package:

> $ go get bitbucket.org/emixdemix/godbrbac
	
Usage
=====

This library creates a set of tables in your database to persist roles, permissions and users:

1. Preparing

2. Checking

Preparing
---------

Import the library:

	import "bitbucket.org/emixdemix/godbrbac"

Checking
--------


Authors
=======

 * Emix Demix <emixdemix@gmail.com> 

Open Source - MIT Software License
==================================

See LICENSE.