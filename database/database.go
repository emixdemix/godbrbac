/*

Engine implementation of MySql storage area. It uses GORM as an ORM framework.

*/
package database

import (
	"bitbucket.org/emixdemix/godbrbac"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// Database object which implements Engine. It has also a reference to a gorm object
type Database struct {
	godbrbac.Engine
	Db *gorm.DB
}

// Implementation of INIT. Uses the connection string to connect to MySql
func (db *Database) Init(connectionString string) {
	database, err := gorm.Open("mysql", connectionString)
	if err != nil {
		db.Db = nil
	} else {
		db.Db = database
	}
}

// Implementation of all the Engine functionality.

func (db *Database) FindPermissionByName(permissionName string, permission *godbrbac.Permissions) {
	db.Db.Where("name=?", permissionName).First(permission)
}

func (db *Database) FindRoleByName(roleName string, role *godbrbac.Roles) {
	db.Db.Where("name=?", roleName).First(role)
}

func (db *Database) CreatePermission(permission *godbrbac.Permissions) error {
	ret := db.Db.Create(permission)
	return ret.Error
}

func (db *Database) CreateRole(role *godbrbac.Roles) error {
	role.Permission = make([]godbrbac.Permissions, 0)
	ret := db.Db.Create(&role)
	return ret.Error
}

func (db *Database) DeletePermission(permission *godbrbac.Permissions) error {
	ret := db.Db.Delete(permission)
	return ret.Error
}

func (db *Database) DeleteRole(role *godbrbac.Roles) error {
	role.Permission = make([]godbrbac.Permissions, 0)
	db.Db.Model(&role).Association("Permission").Clear()
	ret := db.Db.Delete(role)
	return ret.Error
}

func (db *Database) CheckRolesWithPermission(permission *godbrbac.Permissions) int {
	var count int = -1
	db.Db.Table("role_permissions").Select("roles.role_id").Where("permissions_permission_id=?", permission.PermissionId).Count(&count)
	return count
}

func (db *Database) UpdateRole(role *godbrbac.Roles) error {
	ret := db.Db.Save(&role)
	return ret.Error
}

func (db *Database) CreateIdentity(id *godbrbac.Identities) error {
	ret := db.Db.Save(id)
	return ret.Error
}

func (db *Database) UpdateIdentity(id *godbrbac.Identities) error {
	ret := db.Db.Save(id)
	return ret.Error
}

func (db *Database) FindIdentityByUsername(username string, id *godbrbac.Identities) error {
	ret := db.Db.Where("username=?", username).First(id)
	return ret.Error
}

func (db *Database) DeleteIdentity(id *godbrbac.Identities) error {
	db.Db.Model(id).Association("Role").Clear()
	ret := db.Db.Delete(id)
	return ret.Error
}

func (db *Database) CreateTables() {
	db.Db.CreateTable(&godbrbac.Permissions{})
	db.Db.CreateTable(&godbrbac.Roles{})
	db.Db.CreateTable(&godbrbac.Identities{})
}
